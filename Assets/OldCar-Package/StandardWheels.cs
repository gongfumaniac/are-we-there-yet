﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StandardWheels
{
    public Transform[] wheels = new Transform[2];
    public bool motor;
    public bool steering;

    [SerializeField]
    float wheelRadius;

    float maxRotation = 25f;
    float steeringDecay = 120f;

    Quaternion[] startRotation = new Quaternion[2];
    float currentRotationX;

    float currentSteering;

    public void Initiate()
    {
        for (int i = 0; i < 2; i++)
        {
            startRotation[i] = wheels[i].rotation;
        }
    }


    public float Steer(float steering)
    {
        float hInput = Input.GetAxisRaw("Horizontal");

        currentSteering += steering;
        currentSteering = Mathf.Clamp(currentSteering, -maxRotation, maxRotation);

        float currentSteeringSign = Mathf.Sign(currentSteering);

        if(hInput == 0f)
        {
            currentSteering -= steeringDecay * Time.fixedDeltaTime * currentSteeringSign;

            if (Mathf.Abs(currentSteering) < 5f)
            {
                currentSteering = 0f;
            }
        }
        //else if(currentSteeringSign != hInput)
        //{

        //}

        return currentSteering;
    }

    public void Turn(float velocity)
    {
        float turnsPerSecond = velocity * 2 * Mathf.PI;
        currentRotationX += turnsPerSecond;

        if (steering)
        {
            for (int i = 0; i < 2; i++)
            {
                wheels[i].localRotation = Quaternion.Euler(currentRotationX, currentSteering, 0f);
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                wheels[i].localRotation = Quaternion.Euler(currentRotationX, 0f, 0f);
            }
        }
    }
}
