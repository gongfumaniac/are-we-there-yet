﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardCarController : MonoBehaviour
{
    public List<StandardAxleInfo> axleInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;

    void FixedUpdate() //it was public in the tutorial
    {
        float motor = maxMotorTorque * Input.GetAxisRaw("Vertical");
        float steering = maxSteeringAngle * Input.GetAxisRaw("Horizontal");

        foreach (StandardAxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.motor)
            {
                axleInfo.Motor(motor);
            }
            if (axleInfo.steering)
            {
                axleInfo.Steer(steering);
            }

            axleInfo.ApplyLocalPosition();
        }
    }
}
