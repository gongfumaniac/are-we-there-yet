﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StandardAxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;

    public void Steer(float steering)
    {
        leftWheel.steerAngle = steering;
        rightWheel.steerAngle = steering;
    }

    public void Motor(float motor)
    {
        leftWheel.motorTorque = motor;
        rightWheel.motorTorque = motor;
    }

    public void ApplyLocalPosition()
    {
        SetPosition(leftWheel);
        SetPosition(rightWheel);
    }

    void SetPosition(WheelCollider wheel)
    {
        if (wheel.transform.childCount == 0)
        {
            Debug.LogWarning("Wheel has no mesh.");
            return;
        }

        Transform visualWheel = wheel.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;

        wheel.GetWorldPose(out position, out rotation);

        visualWheel.position = position;
        visualWheel.rotation = rotation * Quaternion.Euler(0f, 0f, 90f); //The second quaternion should be deleted as soon as I have acorrectly rotated wheel-prefab!

        Debug.Log(rotation.eulerAngles.ToString());
    }
}
