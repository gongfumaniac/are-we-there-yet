﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardCar : MonoBehaviour
{
    [SerializeField]
    List<StandardWheels> wheels;

    Rigidbody rb;

    float steerSpeed = 150f;
    float acceleration = 50000f;
    float maxSpeed = 15f;

    float turnSpeedMultiplier = 0.5f;

    float sqrMaxSpeed;

    public Vector3 localVelocity;

    public float currentSteering;

    float sidewayDrag = 0.08f;
    float frontalDrag = 0.02f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        sqrMaxSpeed = maxSpeed * maxSpeed;
    }

    private void FixedUpdate()
    {
        Motor();
        RotateBody();
        ApplyDrag();


    }

    private void Motor()
    {
        if (Mathf.Abs(rb.velocity.sqrMagnitude) < sqrMaxSpeed)
        {
            rb.AddForce(transform.forward * acceleration * Input.GetAxis("Vertical"));
        }
    }

    private void RotateBody()
    {
        float turnAmount = currentSteering * Time.deltaTime * rb.velocity.magnitude * turnSpeedMultiplier;
        if(localVelocity.z < 0f)
        {
            turnAmount *= -1;
        }
        print(currentSteering);
        transform.rotation *= Quaternion.Euler(0f, turnAmount, 0f);
    }

    private void ApplyDrag()
    {
        localVelocity = transform.InverseTransformDirection(rb.velocity);

        localVelocity.x *= 1 - sidewayDrag;
        localVelocity.z *= 1 - frontalDrag;

        Vector3 globalVelocity = transform.TransformDirection(localVelocity);
        rb.velocity = globalVelocity;
    }

    private void Update()
    {
        SetWheels();
    }

    private void SetWheels()
    {
        float steering = (Input.GetAxis("Horizontal") * steerSpeed * Time.deltaTime);
        foreach (StandardWheels wheels in wheels)
        {
            currentSteering = wheels.Steer(steering);
            wheels.Turn(localVelocity.magnitude);
        }
    }
}
