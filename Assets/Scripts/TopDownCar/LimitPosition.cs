﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitPosition : MonoBehaviour
{
    [SerializeField]
    LayerMask mask;

    Transform player;
    Rigidbody rb;

    float screenMiddleX;
    RaycastHit hit;

    float limitOffset = 0.75f;

    float positionLimit;
    float bounceOff = 0.6f;
    float maxBounce = 10f;


    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = player.GetComponent<Rigidbody>();

        screenMiddleX = Screen.width / 2f;
    }

    public void CheckLimit(float distanceToLimit)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(screenMiddleX, 0f, 0f));
        if (Physics.Raycast(ray, out hit, 100f, mask))
        {
            positionLimit = hit.point.z;
        }
        else
        {
            Debug.LogWarning("Camera Ray didn't hit anything!");
        }

        if((player.position.z - limitOffset) < positionLimit - distanceToLimit)
        {
            LimitPlayerPosition(distanceToLimit);
        }
    }


    void LimitPlayerPosition(float distanceToLimit)
    {
        Vector3 newPosition = player.position;
        newPosition.z = positionLimit + limitOffset - distanceToLimit;

        player.transform.position = newPosition;

        
        Vector3 newVelocity = Vector3.ClampMagnitude(rb.velocity * -bounceOff, maxBounce);
        rb.velocity = newVelocity;

 
    }
}
