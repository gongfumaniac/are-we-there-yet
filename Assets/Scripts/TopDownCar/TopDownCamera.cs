﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCamera : MonoBehaviour
{
    LimitPosition limit;

    Transform player;
    //Vector3 offset = new Vector3(0f, 55f, -18f);
    Vector3 offset = new Vector3(0f, 20f, -15f);
    //(0f, 35f, -25f) for PC
    //Vector3 startEuler = new Vector3(44, 0f, 0f); 
    Vector3 startEuler = new Vector3(34f, 0f, 0f);

    //(38, 0f, 0f) for PC

    Vector3 dampRef;
    float smoothTime;
    float minSmoothTime = 0.02f;
    float maxSmoothTime = 0.4f;
    float smoothChangeRate = 1f;

    float furthestPosition;
    float maxDistance;
    float backtrackDistance = 20f;

    float limitDistance;

    void Awake()
    {
        limit = GetComponent<LimitPosition>();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        transform.position = offset + Vector3.forward * player.position.z;
        transform.rotation = Quaternion.Euler(startEuler);

        smoothTime = minSmoothTime;
    }

    void FixedUpdate()
    {
        Vector3 targetPosition = transform.position;

        targetPosition.z = player.position.z + offset.z;

        maxDistance = furthestPosition + offset.z - backtrackDistance;

        if (targetPosition.z < maxDistance)
        {
            targetPosition.z = maxDistance;
        }

        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref dampRef, smoothTime);
    }

    private void Update()  //I am not sure if I can put this in update opposed to in FixedUpdate.
    {

        if (player.position.z >= furthestPosition)
        {
            furthestPosition = player.position.z;

            ChangeSmoothTime(minSmoothTime);
        }
        else
        {
            ChangeSmoothTime(maxSmoothTime);

            float distanceToLimit = transform.position.z - (furthestPosition + offset.z - backtrackDistance);

            limit.CheckLimit(distanceToLimit);
        }
    }

    void ChangeSmoothTime(float target)
    {
        if (Mathf.Abs(smoothTime - target) < 0.1f)
        {
            return;
        }

        smoothTime = Mathf.Lerp(smoothTime, target, smoothChangeRate * Time.deltaTime);
    }
}
