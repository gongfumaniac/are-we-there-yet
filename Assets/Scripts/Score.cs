﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField]
    Text scoreText;

    float pointsPerSecond = 10;
    float pointsPerCrash = -10;
    float pointsPerInjury = -15;
    float pointsPerDeath = -30;

    float winBonus;

    public void DisplayScore(float valueLeft, bool hasWon, float winBonus)
    {
        this.winBonus = winBonus;

        if (hasWon) { valueLeft = Mathf.CeilToInt(valueLeft); }

        float timePoints = hasWon ? (pointsPerSecond * valueLeft) : 0f;
        float crashDeduct = pointsPerCrash * CrashData.crashes;
        float injuryDeduct = pointsPerInjury * CrashData.injuries;
        float deathDeduct = pointsPerDeath * CrashData.deaths;

        float total = timePoints + crashDeduct + injuryDeduct + deathDeduct;

        float winningBonus = 0f;
        if (hasWon)
        {
            total += winBonus;
            winningBonus = winBonus;
        }


        scoreText.text = GetScoreText(valueLeft, timePoints, crashDeduct, injuryDeduct, deathDeduct, total, winningBonus, hasWon);
    }

    //string GetCrashData()
    //{
    //    string output = "<b>Crashes: " + CrashData.crashes + "</b>\n\n"
    //+ "<b>Injuries:</b>\n"
    //+ CrashData.injuries + " Severe\n"
    //+ CrashData.deaths + " Fatal";
    //    return output;
    //}

    string GetScoreText(float valueLeft, float timePoints, float crashDeduct, float injuryDeduct, float deathDeduct, float total, float winningBonus, bool hasWon)
    {
        string output = "";
        string scoreColor = "";
        string loseColor = "red";

        if (hasWon)
        {
            output += valueLeft.ToString("0") + " Seconds left: " + timePoints.ToString("0") + " Points\n\n";
            if (total >= 0f)
            {
                scoreColor = "white";
            }
            else
            {
                scoreColor = loseColor;
            }
        }
        else
        {
            scoreColor = loseColor;

            string unit = "m";
            string distanceFormat = "0";
            if (valueLeft > 1000f)
            {
                distanceFormat = "0.00";
                valueLeft /= 1000f;
                unit = "km";
            }
            output += valueLeft.ToString(distanceFormat) + " " + unit + " left.\n\n";
        }

        output += CrashData.crashes + " Crashes: " + crashDeduct.ToString("0") + " Points\n\n"
      + "Injuries:\n"
      + CrashData.injuries + " Severe: " + injuryDeduct.ToString("0") + " Points\n"
      + CrashData.deaths + " Fatal: " + deathDeduct.ToString("0") + " Points\n\n"
      + "Win bonus: " + winningBonus + "\n\n"
      + "<b><color=" + scoreColor + ">Total Points: " + total.ToString("0") + "</color></b>";

        return output;
    }
}
