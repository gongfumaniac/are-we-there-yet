﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//I could make the rotation slower if velocity is low.
//There is no visible steering for the wheels in this script.

public class SimplePlayerCar : MonoBehaviour
{
    [SerializeField]
    List<WheelAxis> axes;

    Rigidbody rb;

    float steerSpeed = 160f; //100f for player_car
    float maxForce = 110000f;
    float minForce = 100000f;
    float force;
    float maxSpeed = 100f;

    float turnSpeedMultiplier = 0.5f;

    float sqrMaxSpeed;

    Vector3 localVelocity;
    float velocitySqrMagnitude;

    float currentSteering;

    float sidewayDrag = 0.75f; //0.1f for player_car
    float frontalDrag = 0.01f;

    float collisionKnockback = 10000f;
    float collisionFriction = 0.2f;


    [Header("For Testing Purposes")]
    //public Image speedBar;

    public float testEuler;

    public float steerDir = 0;

    public Vector3 testEulerMove;



    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        sqrMaxSpeed = maxSpeed * maxSpeed;
    }

    private void FixedUpdate()
    {
        Motor();
        //RotateBody();
        ApplyDrag();
    }

    private void Motor()
    {
        velocitySqrMagnitude = rb.velocity.sqrMagnitude;
        if (velocitySqrMagnitude < sqrMaxSpeed)
        {
            force = Mathf.Lerp(minForce, maxForce, (velocitySqrMagnitude / sqrMaxSpeed)); //the quicker the car is, the mor force is applied to it.
            //rb.AddForce(transform.forward * force * Input.GetAxis("Vertical"));
            rb.AddForce(transform.forward * force * 1f);
        }
    }

    //private void RotateBody()
    //{
    //    float velocityMultiplier = Mathf.Clamp(rb.velocity.magnitude, 0f, 10f);

    //    float turnAmount = currentSteering * Time.deltaTime * velocityMultiplier * turnSpeedMultiplier;
    //    if (localVelocity.z < 0f)
    //    {
    //        turnAmount *= -1;
    //    }
    //    //transform.rotation *= Quaternion.Euler(0f, turnAmount, 0f);
    //    //rb.MoveRotation(rb.rotation * Quaternion.Euler(0f, turnAmount, 0f));
    //}

    private void ApplyDrag()
    {
        localVelocity = transform.InverseTransformDirection(rb.velocity);

        localVelocity.x *= 1 - sidewayDrag;
        localVelocity.z *= 1 - frontalDrag;
        //localVelocity.y = Mathf.Clamp(localVelocity.y, -10f, 0.5f);

        Vector3 globalVelocity = transform.TransformDirection(localVelocity);
        rb.velocity = globalVelocity;

        if (rb.velocity.y > 0.1f)
        {
            //rb.AddForce(Vector3.down * 100000f);
        }
    }

    private void Update()
    {
        SetWheels();
        //SetTestSpeedometer();
    }

    //private void SetTestSpeedometer()
    //{
    //    speedBar.fillAmount = rb.velocity.sqrMagnitude / sqrMaxSpeed;
    //}

    private void SetWheels()
    {
        float input = Input.GetAxisRaw("Horizontal");

        if(input == 0f)
        {
            input = CheckMobileControls();
        }

        //float steering = (input * steerSpeed * Time.deltaTime);
        foreach (WheelAxis wheels in axes)
        {
            float steering = CalculateSteering(input);

            //currentSteering = wheels.Steer(steering, input);
            wheels.Turn(localVelocity.magnitude);
        }
    }

    float CheckMobileControls()
    {
        if (Input.touchCount == 0) { return 0f; }

        if(Input.touches[0].position.x > Screen.width / 2f)
        {
            return 1f;
        }
        else
        {
            return -1f;
        }
    }

    private float CalculateSteering(float input)
    {
        float deadZone = 1f;

        float maxRotation = 30f;
        float minRotation = 360f - maxRotation;

        float currentRotation = transform.eulerAngles.y;
        float targetRotation = 0f;

        testEuler = currentRotation;

        if (input == 0f)
        {
            //Car should steer so that it is rotating to Euler(0f, 0f, 0f)
            //targetRotation stays 0f;
            if (currentRotation < deadZone || currentRotation > 360 - deadZone)
            {
                //transform.eulerAngles = new Vector3(0f, 0f, 0f);
                rb.MoveRotation(Quaternion.identity);
                steerDir = 0;
            }
            else if (currentRotation < 180f)
            {
                steerDir = -1;
            }
            else
            {
                steerDir = 1;

            }

        }
        else if (input > 0f)
        {
            //Car should steer right, so that it is rotating to Euler(0f, minRotation, 0f)
            targetRotation = maxRotation;
            if (currentRotation < maxRotation)
            {
                steerDir = 1;
            }
            else if (currentRotation <= 180f + maxRotation)
            {
                steerDir = -1;
            }
            else //if(currentRotation > 180f + maxRotation)
            {
                steerDir = 1;
            }
        }
        else if (input < 0f)
        {
            targetRotation = minRotation;
            if (currentRotation > minRotation)
            {
                steerDir = -1;
            }
            else if (currentRotation > 180f - maxRotation)
            {
                steerDir = 1;
            }
            else //if(currentRotation < 180f - maxRotation)
            {
                steerDir = -1;
            }
        }

        float steerDistance = Mathf.Abs(currentRotation - targetRotation);
        if (steerDistance > 180) { steerDistance = 360 - steerDistance; }

        rb.MoveRotation(Quaternion.Euler(0f, steerDir * Time.fixedDeltaTime * 1f * steerDistance, 0f) * rb.rotation);
        return steerDir;
    }

    private void OnCollisionEnter(Collision other)
    {
        ContactPoint contact = other.contacts[0];
        float collisionForce = other.relativeVelocity.magnitude;

        collisionForce = Mathf.Clamp(collisionForce, 0f, 100f);


        Vector3 dir = transform.position - contact.point;

        rb.AddForce(dir.normalized * collisionKnockback * collisionForce);

    }

    private void OnCollisionStay(Collision collision)
    {

        float sqrMinMagnitude = 2f;
        float minPercentage = 0.5f;

        if (velocitySqrMagnitude > sqrMinMagnitude)
        {
            float currentSpeedPercentage = velocitySqrMagnitude / sqrMaxSpeed;
            currentSpeedPercentage = Mathf.Clamp(currentSpeedPercentage, minPercentage, 1f);
            rb.velocity *= 1f - (collisionFriction * currentSpeedPercentage);
        }
    }
}

//I need to make sure the car doesn't lift off
//Collision feels very wonky.

