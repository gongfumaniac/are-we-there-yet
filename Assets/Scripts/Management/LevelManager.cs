﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    Transform player;

    int numberOfStreets = 3;
    [SerializeField]
    GameObject streetPrefab;
    Transform[] streetParts;
    int nextPartIndex;
    float streetLength = 300f;

    float nextPosition;
    float nextPositionTrigger;


    [SerializeField]
    GameObject gasStationPrefab;
    Transform gasStation;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        streetParts = new Transform[numberOfStreets];
        CreateElements();
    }

    void CreateElements()
    {
        nextPosition = -streetLength;
        for (int i = 0; i < streetParts.Length; i++)
        {
            streetParts[i] = GameObject.Instantiate(streetPrefab, Vector3.forward * nextPosition, Quaternion.identity, transform).transform;
            nextPosition += streetLength; 
        }

        nextPositionTrigger = GetNextPositionTrigger();

        gasStation = GameObject.Instantiate(gasStationPrefab, Vector3.right * 1000f, Quaternion.identity, transform).transform;
    }

    void Update()
    {
        if(player.position.z >= nextPositionTrigger)
        {
            SetNextPart();
        }
    }

    private void SetNextPart()
    {
        //if(gasStation is next) { setGasStation; set nextPosition and nextPositionTrigger; return;}

        streetParts[nextPartIndex].transform.position = Vector3.forward * nextPosition;
        nextPosition += streetLength;


        nextPositionTrigger = GetNextPositionTrigger();

        nextPartIndex++;
        if(nextPartIndex >= streetParts.Length)
        {
            nextPartIndex = 0;
        }
    }

    float GetNextPositionTrigger()
    {
        return nextPosition - (streetLength * (numberOfStreets -1)) + streetLength / 2f;
    }
}
