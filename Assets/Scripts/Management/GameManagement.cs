﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagement : MonoBehaviour
{
    [SerializeField]
    Transform player;

    //[SerializeField]
    float timeToReachGoal = 60f;

    //[SerializeField]
    float targetDistance = 10000f;

    [SerializeField]
    float travelSpeedModifier = 0.5f;

    [Header("UI")]
    [SerializeField]
    Text distanceDisplay;
    [SerializeField]
    Text timeDisplay;

    [SerializeField]
    GameObject statusDisplay;

    [SerializeField]
    GameObject winDisplay;

    [SerializeField]
    GameObject loseDisplay;

    [SerializeField]
    GameObject buttonParent;

    [SerializeField]
    Text casualtyDisplay;

    bool gameEnded = false;

    float winBonus;

    StatusColor statusCol;

    void Start()
    {
        CopyDifficulty();
        CrashData.Reset();

        statusCol = GetComponent<StatusColor>();

        Screen.orientation = ScreenOrientation.LandscapeLeft;

        winDisplay.SetActive(false);
        loseDisplay.SetActive(false);
        buttonParent.SetActive(false);

        statusDisplay.SetActive(true);

        casualtyDisplay.enabled = false;
    }

    private void CopyDifficulty()
    {
        DifficultySetting dif = DifficultyManager.GetDifficulty();
        timeToReachGoal = dif.time;
        targetDistance = dif.distance;
        winBonus = dif.winBonus;
    }

    void Update()
    {
        if (gameEnded) { return; }

        if(player.position.z > targetDistance)
        {
            WinGame();
        }
        if(Time.timeSinceLevelLoad > timeToReachGoal)
        {
            Lose();
        }

        UpdateDisplays();
        UpdateStatusColor();

        //DisplayCasualties();
    }

    private void UpdateStatusColor()
    {
        float distanceLeft = (targetDistance - player.position.z) * travelSpeedModifier;
        float timeLeft = timeToReachGoal - Time.timeSinceLevelLoad;

        statusCol.UpdateDatas(timeLeft, distanceLeft);
    }

    private void EndGame(Transform parent, bool hasWon)
    {
        //string output = "<b>Crashes: " + CrashData.crashes + "</b>\n\n"
        //    + "<b>Injuries:</b>\n"
        //    + CrashData.injuries + " Severe\n"
        //    + CrashData.deaths + " Fatal";
        //casualtyDisplay.text = output;

        Time.timeScale = 0f;
        gameEnded = true;

        float valueLeft = hasWon ? (timeToReachGoal - Time.timeSinceLevelLoad) : ((targetDistance- player.position.z) * travelSpeedModifier);

        GetComponent<Score>().DisplayScore(valueLeft, hasWon, winBonus);

        casualtyDisplay.enabled = true;
        casualtyDisplay.transform.parent = parent;

        buttonParent.SetActive(true);

    }

    private void UpdateDisplays()
    {
        float distanceToGoal = targetDistance - player.position.z;
        distanceToGoal *= travelSpeedModifier;
        if(distanceToGoal < 0f) { distanceToGoal = 0f; }

        string unit = "";
        string format = "";
        if(distanceToGoal < 1000f)
        {
            unit = "m";
            format = "0";
        }
        else
        {
            unit = "km";
            format = "0.00";
            distanceToGoal /= 1000f;
        }

        timeDisplay.text = (timeToReachGoal - Time.timeSinceLevelLoad).ToString("0") + "s";

        distanceDisplay.text = distanceToGoal.ToString(format) + " " + unit;
    }

    private void WinGame()
    {
        winDisplay.SetActive(true);
        statusDisplay.SetActive(false);


        EndGame(winDisplay.transform, true);
    }


    private void Lose()
    {
        loseDisplay.SetActive(true);
        //statusDisplay.SetActive(false);

        EndGame(loseDisplay.transform, false);
    }
}
