﻿
using UnityEngine;
//using Utilities;
using System.Collections.Generic;

public static class DifficultyManager
{
    static DifficultySetting easy = new DifficultySetting(60, 4000, 200); // 60 4000
    //static DifficultySetting easy = new DifficultySetting(60, 40, 200);

    //static DifficultySetting medium = new DifficultySetting(90, 7000, 300);
    static DifficultySetting medium = new DifficultySetting(90, 7000, 300);

    //static DifficultySetting hard = new DifficultySetting(120, 11000, 500);
    static DifficultySetting hard = new DifficultySetting(115, 11000, 500);

    static DifficultySetting current;

    static Difficulty currentDifficulty = Difficulty.Easy;

    public static List<DifficultyButton> buttons = new List<DifficultyButton>();

    public static DifficultySetting GetDifficulty()
    {
        if (current == null)
        {
            current = easy;
            currentDifficulty = Difficulty.Easy;
        }

        return current;
    }

    public static void SetDifficulty(Difficulty d)
    {
        switch (d)
        {
            case Difficulty.Easy:
                current = easy;
                currentDifficulty = Difficulty.Easy;
                break;

            case Difficulty.Medium:
                current = medium;
                currentDifficulty = Difficulty.Medium;
                break;

            case Difficulty.Hard:
                current = hard;
                currentDifficulty = Difficulty.Hard;
                break;
            default:
                break;
        }


        foreach (DifficultyButton b in buttons)
        {
            b.CheckDifficulty();
        }
    }

    public static void RegisterButton(DifficultyButton b)
    {
        if (buttons.Count == 3) { buttons.Clear(); } // Scene has to have releaded by this time

        buttons.Add(b);
    }
    public static bool HasSameDifficulty(Difficulty d)
    {
        return d == currentDifficulty;
    }

}

//public class DifficultyManager : Singleton<DifficultyManager>
//{
//    [SerializeField]
//    DifficultySetting easy;

//    [SerializeField]
//    DifficultySetting medium;

//    [SerializeField]
//    DifficultySetting hard;

//    DifficultySetting current;

//    public DifficultySetting GetDifficulty()
//    {
//        if (current == null) { current = easy; }

//        return current;
//    }

//    public void SetDifficulty(Difficulty d)
//    {
//        switch (d)
//        {
//            case Difficulty.Easy:
//                current = easy;
//                break;
//            case Difficulty.Medium:
//                current = medium;
//                break;
//            case Difficulty.Hard:
//                current = hard;
//                break;
//            default:
//                break;
//        }
//    }
//}

public enum Difficulty
{
    Easy, Medium, Hard
}

[System.Serializable]
public class DifficultySetting
{
    public float time;
    public float distance;

    public float winBonus;

    public DifficultySetting(float time, float distance, float winBonus)
    {
        this.time = time;
        this.distance = distance;
        this.winBonus = winBonus;
    }
}