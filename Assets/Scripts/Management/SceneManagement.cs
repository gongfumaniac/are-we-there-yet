﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    int gameIndex = 1;
    int titleIndex = 0;


    void Awake()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    public void StartGame()
    {
        SceneManager.LoadScene(gameIndex);

        Time.timeScale = 1f;
    }

    public void LoadTitle()
    {
        SceneManager.LoadScene(titleIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
