﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    float force = 90000f;
    float maxSpeed = 60f;
    float sqrMaxSpeed;

    float gravity = 30000f;

    float playerCrashForceFactor = 14000f;
    float crashForceFactor = 6000f;


    Rigidbody rb;
    float mass = 1000f;
    float postCrashMassMultiplier = 0.75f;

    public bool crashed;

    public void Initialize(bool contrary)
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody>();
        }

        Reset();

        if (sqrMaxSpeed == 0f)
        {
            sqrMaxSpeed = maxSpeed * maxSpeed;
        }
        if(mass == 0f)
        {
            mass = rb.mass;
        }

        rb.velocity = new Vector3(0f, 0f, maxSpeed);
        if (contrary)
        {
            rb.velocity *= -1f;
        }

        rb.angularVelocity = Vector3.zero;
    }




    public void Drive()
    {
        if (crashed)
        {
            //if(rb.velocity.y < 0f)
            //{
            rb.AddForce(Vector3.down * gravity);
            //}
            return;
        }
        if (rb.velocity.sqrMagnitude < sqrMaxSpeed)
        {
            rb.AddForce(transform.forward * force);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            Crash(other, playerCrashForceFactor);

        }
        else if (other.gameObject.tag == "Car")
        {
            if (other.transform.GetComponent<Car>().crashed == true)
            {
                Crash(other, crashForceFactor);
            }
        }
    }

    void Crash(Collision other, float crashForce)
    {
        float magnitude = other.relativeVelocity.magnitude;

        if (!crashed)
        {
            ProduceCrashData(magnitude);
        }

        crashed = true;
        rb.constraints = RigidbodyConstraints.None;

        if(magnitude > 1f)
        {
            Vector3 force = (transform.position - other.transform.position).normalized * crashForce * magnitude;

            rb.AddForceAtPosition(force, other.contacts[0].point);
        }

        rb.mass = mass * postCrashMassMultiplier;

    }

    private void ProduceCrashData(float magnitude)
    {
        int numOfPassengers = UnityEngine.Random.Range(1, 4);

        CrashData.ProduceCrashData(magnitude, numOfPassengers);
    }

    public void Reset()
    {
        crashed = false;
        rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        rb.mass = mass;
        rb.angularVelocity = Vector3.zero;
    }
}

//Apply force to cars the player colides with to make him feel more powerful. Also create counters for injuries and deaths (casualties)
//Restrict the y-axis of the position until the car collides or it may flip over on the edge between streets
//Sphere cast to check if player is in front -> then break
