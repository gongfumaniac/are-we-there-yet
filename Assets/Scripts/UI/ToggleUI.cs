﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleUI : MonoBehaviour {

    [SerializeField]
    GameObject element;

    public void ToggleActivity()
    {
        element.SetActive(!element.activeSelf);
    }
}
