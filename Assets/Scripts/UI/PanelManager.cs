﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    int currentPanel = -1;

    private void Awake()
    {
        SetPanel(-1); //Disable all panels
    }

    public void SetPanel(int index)
    {
        bool isDisabling = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            bool enabled = false;
            if (i == index)
            {
                if (i != currentPanel)
                {
                    enabled = true;
                }
                else
                {
                    isDisabling = true;
                }
            }

            transform.GetChild(i).gameObject.SetActive(enabled);

        }

        if (isDisabling)
        {
            currentPanel = -1;
        }
        else
        {
            currentPanel = index;
        }
    }
}
