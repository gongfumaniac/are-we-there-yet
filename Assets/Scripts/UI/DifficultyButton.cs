﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyButton : MonoBehaviour
{
    [SerializeField]
    Difficulty myDifficulty;

    [SerializeField]
    Color enabledColor = Color.blue;

    Image buttonBG;

    private void Awake()
    {
        buttonBG = GetComponentInChildren<Image>();

        DifficultyManager.RegisterButton(this);
    }

    private void OnEnable()
    {
        CheckDifficulty();
    }

    public void OnSelected()
    {
        DifficultyManager.SetDifficulty(myDifficulty);
    }

    public void CheckDifficulty()
    {
        if (DifficultyManager.HasSameDifficulty(myDifficulty))
        {
            buttonBG.color = enabledColor;
        }
        else
        {
            buttonBG.color = Color.white;

        }
    }
}
