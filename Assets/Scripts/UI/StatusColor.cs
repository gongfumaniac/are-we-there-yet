﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusColor : MonoBehaviour
{
    [SerializeField]
    AnimationCurve adjustmentCurve;

    [SerializeField]
    StatusColorData time;

    [SerializeField]
    StatusColorData distance;

    public void UpdateDatas(float timeLeft, float distanceLeft)
    {
        time.UpdateColor(timeLeft, adjustmentCurve);
        distance.UpdateColor(distanceLeft, adjustmentCurve);
    }
}

[System.Serializable]
public class StatusColorData
{
    public Color color;
    public float limit;
    public Image img;

    public void UpdateColor(float value, AnimationCurve adjustmentCurve)
    {
        if(value < limit)
        {
            float percentage = 1-(value / limit);
            float adjustedPercentage = adjustmentCurve.Evaluate(percentage);

            img.color = Color.Lerp(Color.white, color, adjustedPercentage);
        }
    }
}
