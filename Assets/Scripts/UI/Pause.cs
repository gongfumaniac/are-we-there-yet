﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField]
    GameObject PauseMenu;

    [SerializeField]
    GameObject PauseDifficultySettings;

    bool isPaused;

    private void Awake()
    {
        PauseMenu.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnPause();
        }
    }

    public void OnPause()
    {
        if (isPaused) { OnContinue(); return; }

        PauseDifficultySettings.SetActive(false);
        PauseMenu.SetActive(true);
        //StatusUI.SetActive(false);

        Time.timeScale = 0f;

        isPaused = true;
    }

    public void OnContinue()
    {
        PauseMenu.SetActive(false);
        //StatusUI.SetActive(true);

        Time.timeScale = 1f;
        isPaused = false;
    }
}
