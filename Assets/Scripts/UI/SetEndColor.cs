﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetEndColor : MonoBehaviour {

    public Color easyColor;
    public Color mediumColor;
    public Color hardColor;

    Image img;

    private void Awake()
    {
        img = GetComponentInChildren<Image>();
    }

    private void OnEnable()
    {
        if (DifficultyManager.HasSameDifficulty(Difficulty.Easy))
        {
            img.color = easyColor;
        }
        if (DifficultyManager.HasSameDifficulty(Difficulty.Medium))
        {
            img.color = mediumColor;
        }
        if (DifficultyManager.HasSameDifficulty(Difficulty.Hard))
        {
            img.color = hardColor;
        }
    }
}
