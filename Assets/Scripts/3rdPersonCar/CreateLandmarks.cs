﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLandmarks : MonoBehaviour
{
    [SerializeField]
    float offsetDistance;

    void Start()
    {
        for (int i = 1; i < 10; i++)
        {
            for (int j = 1; j < 10; j++)
            {
                Transform cube = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                cube.transform.position = new Vector3(i * offsetDistance, 0f, j * offsetDistance);
                cube.localScale = new Vector3(1f, 10f, 1f);
            }
        }
    }
}
