﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerCar : MonoBehaviour
{
    [SerializeField]
    List<WheelAxis> axes;

    Rigidbody rb;

    float steerSpeed = 160f; //100f for player_car
    float maxForce = 120000f;
    float minForce = 100000f;
    float force;
    float maxSpeed = 100f;

    float turnSpeedMultiplier = 0.5f;

    float sqrMaxSpeed;

    Vector3 localVelocity;

    float currentSteering;

    float sidewayDrag = 0.75f; //0.1f for player_car
    float frontalDrag = 0.01f;

    float collisionKnockback = 10000f;

    [Header("For Testing Purposes")]
    public Image speedBar;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        sqrMaxSpeed = maxSpeed * maxSpeed;
    }

    private void FixedUpdate()
    {
        Motor();
        RotateBody();
        ApplyDrag();
    }

    private void Motor()
    {
        if (Mathf.Abs(rb.velocity.sqrMagnitude) < sqrMaxSpeed)
        {
            force = Mathf.Lerp(minForce, maxForce, (rb.velocity.sqrMagnitude / sqrMaxSpeed)); //the quicker the car is, the mor force is applied to it.
            rb.AddForce(transform.forward * force * Input.GetAxis("Vertical"));
        }
    }

    private void RotateBody()
    {
        float velocityMultiplier = Mathf.Clamp(rb.velocity.magnitude, 0f, 10f);

        float turnAmount = currentSteering * Time.deltaTime * velocityMultiplier * turnSpeedMultiplier;
        if (localVelocity.z < 0f)
        {
            turnAmount *= -1;
        }
        //transform.rotation *= Quaternion.Euler(0f, turnAmount, 0f);
        rb.MoveRotation(rb.rotation * Quaternion.Euler(0f, turnAmount, 0f));
    }

    private void ApplyDrag()
    {
        localVelocity = transform.InverseTransformDirection(rb.velocity);

        localVelocity.x *= 1 - sidewayDrag;
        localVelocity.z *= 1 - frontalDrag;
        //localVelocity.y = Mathf.Clamp(localVelocity.y, -10f, 0.5f);

        Vector3 globalVelocity = transform.TransformDirection(localVelocity);
        rb.velocity = globalVelocity;

        if (rb.velocity.y > 0.1f)
        {
            //rb.AddForce(Vector3.down * 100000f);
        }
    }

    private void Update()
    {
        SetWheels();
        SetTestSpeedometer();
    }

    private void SetTestSpeedometer()
    {
        if (speedBar != null)
        {
            speedBar.fillAmount = rb.velocity.sqrMagnitude / sqrMaxSpeed;
        }
    }

    private void SetWheels()
    {
        float input = Input.GetAxisRaw("Horizontal");
        float steering = (input * steerSpeed * Time.deltaTime);
        foreach (WheelAxis wheels in axes)
        {

            currentSteering = wheels.Steer(steering, input);
            wheels.Turn(localVelocity.magnitude);
        }
    }

    //private void OnCollisionEnter(Collision other) //Console displayed errors even when the script was inactive so I commented this out.
    //{
    //    ContactPoint contact = other.contacts[0];
    //    float collisionForce = other.relativeVelocity.magnitude;

    //    collisionForce = Mathf.Clamp(collisionForce, 0f, 100f);


    //    Vector3 dir = transform.position - contact.point;

    //    rb.AddForce(dir.normalized * collisionKnockback * collisionForce);
    //}
}

//I need to make sure the car doesn't lift off
//Collision feels very wonky.
