﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WheelAxis
{
    public Transform[] wheels = new Transform[2];
    public bool steering;

    [SerializeField]
    float wheelRadius;

    float maxSteering = 15f; 
    float steeringDecay = 300f; //250f before
    float deadZone = 5f; //When the steering is set to 0f
    float turnMultiplier = 3f; //How much quicker steering is when changing from one direction to the other


    Quaternion[] startRotation = new Quaternion[2];
    float currentRotationX;

    float currentSteering;

    public void Initiate()
    {
        for (int i = 0; i < 2; i++)
        {
            startRotation[i] = wheels[i].rotation;
        }
    }


    public float Steer(float steering, float hInput)
    {
        currentSteering += steering;
        currentSteering = Mathf.Clamp(currentSteering, -maxSteering, maxSteering);

        float currentSteeringSign = Mathf.Sign(currentSteering);

        if(hInput == 0f)
        {
            float absSteering = Mathf.Abs(currentSteering);
            float steeringPercentage = absSteering / maxSteering;

            float decayFactor = Mathf.Clamp(steeringPercentage, 0.75f, 1f);

            currentSteering -= steeringDecay * Time.fixedDeltaTime * currentSteeringSign * decayFactor;

            if (absSteering < deadZone)
            {
                currentSteering = 0f;
            }
        }
        else if (currentSteeringSign != hInput)
        {
            currentSteering += steering * turnMultiplier;
        }

        return currentSteering;
    }

    public void Turn(float velocity)
    {
        //float turnsPerSecond = velocity * 2 * Mathf.PI;
        //if(velocity < 0.01f) { return; }

        float turnsPerSecond = velocity / (wheelRadius * 2 * Mathf.PI);
        currentRotationX += turnsPerSecond * 360 * Time.deltaTime;

        if (steering)
        {
            for (int i = 0; i < 2; i++)
            {
                wheels[i].localRotation = Quaternion.Euler(currentRotationX, currentSteering, 0f);
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                wheels[i].localRotation = Quaternion.Euler(currentRotationX, 0f, 0f);
            }
        }
    }
}
