﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    Transform target;

    Vector3 offset = new Vector3(0f, 4f, -10);

    Vector3 dampRef;

    void FixedUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, target.position - target.InverseTransformDirection(offset), ref dampRef, 8f* Time.fixedDeltaTime);

        Vector3 distance = target.position - transform.position;
        distance.y = transform.position.y - 10f;

        //Quaternion targetRotation = Quaternion.LookRotation(distance.normalized);
        //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.fixedDeltaTime);

        Debug.DrawRay(target.transform.position, target.InverseTransformDirection(offset).normalized * 5f, Color.black);
        //Debug.DrawRay(target.transform.position, offset.normalized * 2f, Color.black);
    }
}
