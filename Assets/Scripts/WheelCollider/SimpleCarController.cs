﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCarController : MonoBehaviour
{
    public List<AxleInfo> axleInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;

    void FixedUpdate() //it was public in the tutorial
    {
        float motor = maxMotorTorque * Input.GetAxisRaw("Vertical");
        float steering = maxSteeringAngle * Input.GetAxisRaw("Horizontal");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.motor)
            {
                axleInfo.Motor(motor);
            }
            if (axleInfo.steering)
            {
                axleInfo.Steer(steering);
            }

            axleInfo.ApplyLocalPosition();
        }
    }
}
