﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RenderTexToPNG : MonoBehaviour
{

    [SerializeField]
    RenderTexture rt;

    [SerializeField]
    string pngOutPath;

    private void Update()
    {
        if (Time.time > 1f)
        {
            DumpRenderTexture();
            this.enabled = false;
        }
    }

    public void DumpRenderTexture()
    {
        var oldRT = RenderTexture.active;

        var tex = new Texture2D(rt.width, rt.height);
        RenderTexture.active = rt;
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        tex.Apply();

        File.WriteAllBytes(Application.dataPath +"/" +  pngOutPath + ".png", tex.EncodeToPNG());
        RenderTexture.active = oldRT;
    }

    //void DumpRenderTexture()
    //{
    //    //var oldRT = RenderTexture.active;

    //    var tex = new Texture2D(rt.width, rt.height);
    //    //RenderTexture.active = rt;
    //    tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
    //    tex.Apply();

    //    File.WriteAllBytes(pngOutPath, tex.EncodeToPNG());
    //    //RenderTexture.active = rt;
    //}
}
