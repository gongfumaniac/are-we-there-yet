﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CrashData
{
    public static int crashes;
    public static int injuries;
    public static int deaths;

    static float deathMag = 140f;
    static float injurMag = 100f;
    static float lightInjurMag = 50f;

    static float magDiversion = 0.2f;

    public static void Reset()
    {
        crashes = 0;
        injuries = 0;
        deaths = 0;
    }

    public static void ProduceCrashData(float magnitude, int numOfPassengers)
    {
        crashes++;

        for (int i = 0; i < numOfPassengers; i++)
        {
            float randomDiversion = Random.Range(-magDiversion, magDiversion);
            float divertedMagnitude = magnitude * (randomDiversion + 1);

            if (magnitude > deathMag)
            {
                deaths++;
            }
            else if (magnitude > injurMag)
            {
                injuries++;
            }
            else if (magnitude > lightInjurMag)
            {
                if (Random.Range(0, 2) > 0)
                {
                    injuries++;
                }
            }
        }
    }
}
