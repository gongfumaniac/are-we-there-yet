﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPooler
{
    GameObject carPrefab;
    Stack<GameObject> pool;
    int poolSize;

    Material[] mats;

    GameObject parent;

    public CarPooler(GameObject carPrefab, int poolSize, Material[] carMats)
    {
        this.carPrefab = carPrefab;
        this.poolSize = poolSize;
        mats = carMats;

        CreatePool();
    }

    void CreatePool()
    {
        parent = new GameObject();

        pool = new Stack<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
           CreateCar();
        }
    }

    GameObject CreateCar()
    {
        GameObject car = GameObject.Instantiate(carPrefab, parent.transform);

        pool.Push(car);
        car.SetActive(false);
        return car;
    }

    public GameObject GetCar()
    {

        if(pool.Count == 0)
        {
            CreateCar();
        }
        GameObject car = pool.Pop();

        ChangeCarColor(car);

        //car.GetComponent<Car>().Reset();

        return car;
    }

    void ChangeCarColor(GameObject car)
    {
        int randomNum = Random.Range(0, mats.Length);
        Material randomMat = mats[randomNum];

        Renderer rend = car.GetComponentInChildren<Renderer>();
        Material[] currentMats = rend.materials;
        currentMats[0] = mats[randomNum];
        rend.materials = currentMats;
    }

    public void ReturnCar(GameObject car)
    {
        car.SetActive(false);
        pool.Push(car);
    }
}
