﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// I need to spawn forward moving cars in front of player as well.

public class Spawner : MonoBehaviour
{
    [SerializeField]
    GameObject carPrefab;

    [SerializeField]
    Material[] carMats;

    [SerializeField]
    Lane[] lanes;



    Transform carContainer;

    private float CamPos
    {
        get { return cam.position.z; }
    }
    Transform cam;

    public float frontSpawnOffset;
    public float backSpawnOffset;

    float spawnOffsetBuffer = 20f;

    public CarPooler pooler;
    int poolSize = 20;

    float timer;
    float spawnInterval;

    List<Car> cars;
    List<Transform> toDespawn = new List<Transform>();

    float ySpawnPos = 2.4f;

    float zMinDistance = 18f;
    float minCarDistance = 6f;
    float sqrMinCarDistance;

    float despawnBuffer = 30f;



    void Start()
    {
        cam = Camera.main.transform;

        pooler = new CarPooler(carPrefab, poolSize, carMats);
        foreach (Lane lane in lanes)
        {
            lane.Initialize(this);
        }
        carContainer = new GameObject().transform;
        carContainer.name = "CarContainer";

        cars = new List<Car>();
        toDespawn = new List<Transform>();

        SetOffsets();

        sqrMinCarDistance = minCarDistance * minCarDistance;
    }

    void SetOffsets()
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height, 0f));
        frontSpawnOffset = GetOffset(ray);

        ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, 0f, 0f));
        backSpawnOffset = GetOffset(ray);

    }

    float GetOffset(Ray ray)
    {
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            return hit.point.z - cam.transform.position.z;
        }
        else
        {
            //Debug.LogWarning("Camera ray hit nothing");
            Debug.LogError("Camera ray hit nothing");
            return 0f;
        }
    }

    public void SpawnCar(Lane lane, bool front)
    {
        Vector3 pos = GetSpawnPosition(lane, front);

        foreach (Transform trans in lane.carTransforms)
        {
            float zDistance = Mathf.Abs(trans.position.z - pos.z);
            if (zDistance < zMinDistance)
            {
                return;
            }
        }
        foreach (Lane l in lanes)
        {
            if (l == lane) { continue; }

            foreach (Transform t in l.carTransforms)
            {
                float sqrDistance = (pos - t.position).sqrMagnitude;

                if (sqrDistance < sqrMinCarDistance)
                {
                    return;
                }
            }
        }

        Transform carTransform = pooler.GetCar().transform;
        carTransform.position = pos;
        carTransform.rotation = GetSpawnRotation(lane.contrary);
        carTransform.gameObject.SetActive(true);

        Car car = carTransform.GetComponent<Car>();
        car.Initialize(lane.contrary);
        cars.Add(car);
        lane.carTransforms.Add(carTransform);
    }


    Vector3 GetSpawnPosition(Lane lane, bool front)
    {
        float zPos = 0f;
        if (lane.contrary || front)
        {
            zPos = cam.position.z + frontSpawnOffset + spawnOffsetBuffer;
        }
        else
        {
            zPos = cam.position.z + backSpawnOffset - spawnOffsetBuffer;
        }

        Vector3 pos = new Vector3(lane.xPos, ySpawnPos, zPos);
        return pos;
    }

    private Quaternion GetSpawnRotation(bool contrary)
    {
        Quaternion rot = Quaternion.identity;

        if (contrary)
        {
            rot = Quaternion.Euler(0f, 180f, 0f);
        }

        return rot;
    }

    private void FixedUpdate()
    {
        //Step through cars (physics)
        foreach (Car car in cars)
        {
            car.Drive();
        }
    }

    void Update()
    {
        foreach (Lane lane in lanes)
        {
            lane.UpdateLane();
        }

        DespawnCheck();
    }

    private void DespawnCheck()
    {

        foreach (Lane lane in lanes)
        {
            toDespawn.Clear();
            foreach (Transform t in lane.carTransforms)
            {
                if (t.position.z < (cam.position.z + backSpawnOffset - despawnBuffer - spawnOffsetBuffer) || t.position.z > cam.position.z + frontSpawnOffset + despawnBuffer + spawnOffsetBuffer)
                {
                    toDespawn.Add(t);
                    //Despawn(lane, t);
                }
            }

            DespawnList(lane, toDespawn);
        }
    }

    private void DespawnList(Lane l, List<Transform> tList)
    {
        foreach(Transform t in tList)
        {
            //print("despawned at Pos:" + t.position.z);
            l.carTransforms.Remove(t);
            pooler.ReturnCar(t.gameObject);
            Car car = t.GetComponent<Car>();
            car.crashed = false;
            cars.Remove(car);
        }
    }

    //void Despawn(Lane l, Transform t)
    //{
    //    if (l.carTransforms.Contains(t))
    //    {
    //        print(t.name + " is Contained.");
    //        l.carTransforms.Remove(t);
    //    }
    //    else
    //    {
    //        print(t.name + " isn't Contained.");

    //    }
    //    //l.carTransforms.Remove(t);
    //    //cars.Remove(t.GetComponent<Car>());
    //    pooler.ReturnCar(t.gameObject);

    //}
}

//Note: Ask jan why I had to create a toDespawn List.


