﻿using UnityEngine;
using System.Collections.Generic;


[System.Serializable]
public class Lane
{
    public bool contrary;
    public float xPos;
    public Vector2 intervalRange;
    public float speed;
    Spawner spawner;

    public List<Transform> carTransforms;

    float timer;
    float currentInterval;

    float frontTimer;
    float frontCurrentInterval;

    int maxCars = 5;

    public void Initialize(Spawner spawnScript)
    {
        carTransforms = new List<Transform>();

        spawner = spawnScript;

        currentInterval = Random.Range(intervalRange.x, intervalRange.y);
        frontCurrentInterval = Random.Range(intervalRange.x, intervalRange.y);

        if (speed == 0f)
        {
            speed = 1f;
        }
    }

    public void UpdateLane()
    {
        timer += Time.deltaTime;
        if (timer >= currentInterval)
        {
            if (carTransforms.Count < maxCars)
            {
                spawner.SpawnCar(this, false);
            }
            timer = 0f;
            currentInterval = Random.Range(intervalRange.x, intervalRange.y);
        }

        if (!contrary)
        {
            UpdateFrontLane();
        }
    }

    void UpdateFrontLane()
    {
        frontTimer += Time.deltaTime;

        if (frontTimer >= frontCurrentInterval)
        {
            if (carTransforms.Count < maxCars)
            {
                spawner.SpawnCar(this, true);
            }

            currentInterval = Random.Range(intervalRange.x, intervalRange.y);
            frontTimer = 0f;
        }
    }
}
